
$(function(){
  $("[data-toggle='tooltip']").tooltip();
  $('.carousel').carousel({ interval:6000})
  $("[data-toggle='popover']").popover();
}) 
  $('#mimodal').on('show.bs.modal', function(e){
    console.log('el modal se está mostrando')

    $('#holabtn').addClass('btn-outline-success')
    $('#holabtn').prop('disabled', true)
    $('#holabtn2').addClass('btn-outline-success')
    $('#holabtn2').prop('disabled', true)
    $('#holabtn3').addClass('btn-outline-success')
    $('#holabtn3').prop('disabled', true)
    $('#holabtn4').addClass('btn-outline-success')
    $('#holabtn4').prop('disabled', true)
  })

  $('#mimodal').on('hide.bs.modal', function(e){
    console.log('el modal se está cerrando')
    $('#holabtn').prop('disabled', false)
    $('#holabtn').removeClass('btn-outline-success')
    $('#holabtn2').prop('disabled', false)
    $('#holabtn2').removeClass('btn-outline-success')
    $('#holabtn3').prop('disabled', false)
    $('#holabtn3').removeClass('btn-outline-success')
    $('#holabtn4').prop('disabled', false)
    $('#holabtn4').removeClass('btn-outline-success')
  })
  $('#mimodal').on('shown.bs.modal', function(e){
    console.log('el modal se mostró')
  })
  $('#mimodal').on('hidden.bs.modal', function(e){
    console.log('el modal se cerró')
  })